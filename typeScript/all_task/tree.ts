class Nodes {
    value;
    left: null | Nodes;
    right: null | Nodes;
    constructor(value: number) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
}

class Tree {
    root: null | Nodes ;
    constructor() {
        this.root = null;
    }

    insert(value: number) {
        let newNode = new Nodes(value);

        if ( !this.root ) {
            this.root = newNode;
            return;
        }

        let root = this.root;

        while ( root ) {
            if ( newNode.value < root.value ) {
                if ( !root.left ) {
                    root.left = newNode;
                    return;
                }

                root = root.left;
            } else {
                if (!root.right) {
                    root.right = newNode;
                    return;
                }

                root = root.right;
            }
        }
    }

    research(element: (Nodes | null), callBack: Function) {

        if ( !element ) {
            return;
        }

        this.research(element.left, callBack);

        if ( callBack ) {
            callBack(element);
        }
        this.research(element.right, callBack);

    }

    sort(callBack: Function, metod: string) {

        if ( metod === 'research' ) {
            return this.research(this.root, callBack);
        }
    }

    search(element: (Nodes | null), value: number): (Nodes | null) {

        if ( element === null ) {
            return null;
        } else if ( value < element.value ) {
            return this.search(element.left, value);
        } else if  ( value > element.value ) {
            return this.search(element.right, value)
        } 

        return element;
    }

    findMin(element: (null | Nodes)) {
        if ( element!.left === null )
        return element;
    }

    remove(element: (Nodes | null), value: number) {

        if ( element === null ) {
            return null;
        } else if ( value < element.value ) {
            element.left = this.remove(element.left, value);
            return element;
        } else if ( value > element.value ) {
            element.right = this.remove(element.right, value);
            return element;
        } else {
            if ( element.left === null && element.right === null ) {
                element = null;
                return element;
            }

            if ( element.left === null ) {
                element = element.right;
                return element;
                
            } else if ( element.right === null ) {
                element = element.left;
                return element
            }
        }
        let newNode = this.findMin(element.right);
        element.value = newNode!.value;
        element.right = this.remove(element.right, newNode!.value);
        return element;
    }
    
}

let binarTree = new Tree();

binarTree.insert(20);
binarTree.insert(7);
binarTree.insert(8);
binarTree.insert(15);
binarTree.insert(35);
binarTree.insert(4);
binarTree.insert(21);
binarTree.insert(2);
binarTree.insert(32);
binarTree.insert(62);
