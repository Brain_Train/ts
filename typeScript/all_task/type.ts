let anagram = (firstlLine: string, secondLine: string) => {
    let string = '';

    if ( firstlLine.length == secondLine.length ) {
        for ( let i = 0; i < firstlLine.length; i++ ) {
            for  (let j = 0; j < secondLine.length; j++ ) {
                if ( firstlLine[i] == secondLine[j] ) {
                    string += firstlLine[i]
                    break;
                }
            }
        }
    } else {
        return false;
    }

    return firstlLine == string;
}

let findEqualNumbers = (num: number) => {
    let currentNumers = num.toString().split('');
    let repeaters: Record<string, number> = {};

    for (let i in currentNumers) {
        repeaters[currentNumers[i]] = repeaters[currentNumers[i]] || 0;
        repeaters[currentNumers[i]]++;
    }
    console.log(currentNumers);
    return repeaters;
}

let numberOfUniqueWords = (sentence: string) => {
    let currentWords = sentence.split(' ');
    let repeaters: Record<string, number> = {};

    for (let i in currentWords) {
        repeaters[currentWords[i]] = repeaters[currentWords[i]] || 0;
        repeaters[currentWords[i]]++;
    }

    let arrayOfValues = Object.values(repeaters);
    let unique = 0;

    for (let i = 0; i < arrayOfValues.length; i++) {
        if (arrayOfValues[i] == 1) {
            unique += 1;
        }
    }

    return unique;
}

let findEqualWords = (sentence: string) => {
    let currentWords = sentence.split(' ');
    let repeaters: Record<string, number> = {};

    for (let i in currentWords) {
        repeaters[currentWords[i]] = repeaters[currentWords[i]] || 0;
        repeaters[currentWords[i]]++;
    }

    return repeaters;
}

let fibonachiCount = (n: number) => {
    let fibonachi = [0, 1];

    if ( n == 0 ) {
        return fibonachi = [0];
    }

    for ( let i = 2; i < n; i++ ) {
        fibonachi[i] = fibonachi[i - 1] + fibonachi[i - 2];
    }
    return fibonachi;
}

function factorial(n: number) {
    let total = 1;

    for ( let i = 0; i < n; i++ ) {
        total = total + (n - 1)
    }
    return total;
}


let shiftingToBinary = (num: number) => {
    let out: string = '';
    let bit: number = 1;

    while (num >= bit) {
        out = (num & bit ? 1 : 0) + out;
        bit <<= 1;
    }

    return out || "0";
}

let sumPositiveOddNumInTwoDementionArray = (array: number[][], cb: Function) => {
    let res: number = 0;

    for ( let i = 0; i < array.length; i++ ) {
        for ( let j = 0; j < array[i].length; j++ ) {
            if ( cb(array[i][j]) ) {
                res += array[i][j];
            }
        }
    }

    return res;
}

const sumMultipleN = (min: number, max: number, cb: Function) => {
    let res: number = 0;

    for ( let i = min; i <= max; i++ ) {
        if ( cb(i) ) {
            res += i;
        }
    }

    return res;
}

let transposeMatrix = (matrix: number[][]) => {
    let transpose = [];

    for ( let i = 0; i < matrix[0].length; i++ ) {
        let arr = [];
        for ( let j = 0; j < matrix.length; j++ ) {
            arr.push(matrix[j][i]);
        }
        transpose.push(arr);
    }
    return transpose;
}

let deleteMatrixRowWithZeros = (matrix: number[][]) => {
    for ( let i = 0; i < matrix.length; i++ ) {
        for ( let j = 0; j < matrix[i].length; j++ ) {
            if (matrix[i][j] == 0) {
                matrix.splice(i, 1);
                break;
            }
        }
    }
    return matrix;
}

function sumMatrix(matrixOne: number[][], matrixTwo: number[][]) {
    let res: number[][] = [];

    for ( let i = 0; i < matrixOne.length; i++ ) {
        res[i] = [];
        for (let j = 0; j < matrixOne[0].length; j++) 
            res[i][j] = matrixOne[i][j] + matrixTwo[i][j];
    }

    return res;
}

let deleteMtarixColumWithZeros = (matrix: number[][]) => {
    let columDelete = (j: number) => {
        for ( let i = 0; i < matrix.length; i++ ) {
            matrix[i].splice(j, 1);
        }
    }

    for ( let i = 0; i < matrix.length; i++ ) {
        for ( let j = 0; j < matrix[i].length; j++ ) {
            if (matrix[i][j] == 0) {
                columDelete(j);
                break;
            }
        }
    }

    return matrix;
}
