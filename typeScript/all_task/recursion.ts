let recutsionFindEqualNumbers = (nums: number, index?: number, obj?: Record<string, number>): Record<string, number> => {
    index = index || 0;
    obj = obj || {};
    let massNums = nums.toString().split('');

    if (massNums.length > index) {
        obj[massNums[index]] = ++obj[massNums[index]] || 1;
        return recutsionFindEqualNumbers(nums, ++index, obj)
    }

    return obj;
}

let recutsionNumberOfUniqueWords = (str: string, index?: number, obj?: Record<string, number>): number => {
    index = index || 0;
    obj = obj || {};
    let strings = str.split(' ');

    if (strings.length > index) {
        obj[strings[index]] = ++obj[strings[index]] || 1;
        return recutsionNumberOfUniqueWords(str, ++index, obj);
    }
    let arrayOfValues = Object.values(obj);
    let counter = 0;

    for (let i = 0; i < arrayOfValues.length; i++) {
        if (arrayOfValues[i] === 1) {
            counter += 1;
        }
    }

    return counter;
}

let recutsionFindEqualWords = (str: string, index?: number, obj?: Record<string,number>) => {
    index = index || 0;
    obj = obj || {};
    let strings = str.split(' ');

    if (strings.length > index) {
        obj[strings[index]] = ++obj[strings[index]] || 1;
        recutsionFindEqualWords(str, ++index, obj);
    }

    return obj;
}

function recursionFactorial(n: number): number {
    if (n === 1) {
        return 1;
    } return n * recursionFactorial(n - 1)
}

let recursionAnountOfZero = (mass: number[], zero?: number, index?: number): number => {
    zero = zero || 0;
    index = index || 0;

    if (mass.length > index) {
        if (mass[index] == 0) {
            zero++;
            return recursionAnountOfZero(mass, zero, ++index);
        } else {
            zero = recursionAnountOfZero(mass, zero, ++index);
        }
    }

    return zero;
}

let recursionAmountOfNegative = (mass: number[], negative?: number, index?: number): number => {
    negative = negative || 0;
    index = index || 0;

    if (mass.length > index) {
        if (mass[index] < 0) {
            negative++;
            return recursionAmountOfNegative(mass, negative, ++index);
        } else {
            negative = recursionAmountOfNegative(mass, negative, ++index)
        }
    }

    return negative;
}

let recursionAmountOfPositive = (mass: number[], positive?: number, index?: number): number => {
    positive = positive || 0;
    index = index || 0;

    if (mass.length > index) {
        if (mass[index] > 0) {
            positive++;
            return recursionAmountOfPositive(mass, positive, ++index);
        } else {
            positive = recursionAmountOfPositive(mass, positive, ++index)
        }
    }

    return positive;
}

let recursionAllMinMaxSumm = (min: number, max: number) => {
    let sum = 0;

    if (++min <= max) {
        sum = recursionAllMinMaxSumm(min, max) + min;

    }
    return sum;

}

let recursionMinMaxSumMultiples = (min: number, max: number, sum?: number): number => {
    sum = sum || 0;

    if (++min <= max) {
        if (min % 3 == 0) {
            sum += min;
        }
        sum = recursionMinMaxSumMultiples(min, max, sum)
    }

    return sum;
}

let recursionDeleteMtarixColumWithZeros = (mass: number[][], index?: number) => {
    index = index || 0;

    let deleteСolumn = (j: string | number) => {
        for (let index in mass) {
            mass[index].splice(+j, 1);
        }
    }
    if (index++ < mass.length) {
        for (let j in mass[index]) {
            if (mass[index][j] == 0) {
                deleteСolumn(j);
                break;
            }
        }
        recursionDeleteMtarixColumWithZeros(mass, index);
    }
    return mass;
}

let recurtionDeleteMatrixRowWithZeros = (matrix: number[][], index?: number) => {
    index = index || 0;

    if (index++ < matrix.length) {
        for (let j in matrix[index]) {
            if (matrix[index][j] == 0) {
                matrix.splice(index, 1);
                break;
            }
        }
        recurtionDeleteMatrixRowWithZeros(matrix, index);
    }
    return matrix;
}

let recursiveTheAverageOfArrayElement = (mass: number[]) => {
    let massFlat = mass.flat();
    let res = {
        even: 0,
        odd: 0,
    };

    let parsingArray = (massFlat: number[], counter?: number) => {
        counter = counter || 0
        if (massFlat.length > counter) {
            if (massFlat[counter] % 2 == 0) {
                res.even += massFlat[counter] / 2;
            } else {
                res.odd += massFlat[counter] / 2;
            }
            parsingArray(massFlat, ++counter)
        }
    }

    parsingArray(massFlat);
    return res;
}
