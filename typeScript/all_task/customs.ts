Object.defineProperty(Array.prototype, 'itsMyMap', {
    value: function (fn: Function) {
        let arr = [];

        for ( let i = 0; i < this.length; i++ ) {
            arr.push(fn(this[i], i, this));
        }

        return arr;
    }
});

Object.defineProperty(Array.prototype, 'itsMyFillter', {
    value: function (fn: Function) {
        const filtered = [];

        for ( let i = 0; i < this.length; i++ ) {
            if (fn(this[i])) {
                filtered.push(this[i]);
            }
        }

        return filtered;
    }
});

Object.defineProperty(Array.prototype, 'itsMyCall', {
    value: function (fn: Record<number | string | symbol, Function>, ...allarg: number[] | string[]) {
        let symbol = Symbol();
        fn[symbol] = this;
        let result = fn[symbol](...allarg);
        delete fn[symbol];
        return result;
    }
});

Object.defineProperty(Array.prototype, 'itsMyBind', {
    value: function (fn: Record<number | string | symbol, Function>, ...allarg: number[] | string[]) {
        let object = {
            ...fn,
        };
        let symbol: symbol = Symbol();
        object[symbol] = this;

        return function (...rest: number[]) {
            let result = object[symbol](...allarg, ...rest);
            delete object[symbol];
            return result;
        };
    }
});


Object.defineProperty(Array.prototype, 'itsMyForEach', {
    value: function (fn: Function, index: number) {
        index = index || 0;

        if (index < this.length) {
            fn(this[index], this);
            this.itsMyForEach(fn, ++index);
        };
    }
});

Object.defineProperty(Array.prototype, 'itsMyReduce', {
    value: function (cb: Function, accumulator: number) {
        let startIndex = 0;
        if (accumulator == undefined) {
            startIndex = 1;
            accumulator = this[0];
        }
        for ( let i = startIndex; i < this.length; i++ ) {
            accumulator = cb(accumulator, this[i], i, this);
        }
        return accumulator
    }
});

function* fibonachiGenerator(n: number, current: number, next: number): Generator {
    current = current || 0;
    next = next || 1;

    if (n == 0) {
        return current;
    }

    yield current 
    yield* fibonachiGenerator(n - 1, next, current + next);
};

let fibonachiIterator = {
    a: 0,
    b: 1,
    n: 10,
    [Symbol.iterator]() {
        let current = this.a;
        let next = this.b;
        let n = this.n;
        let arr = [current, next];
        return {
            next() {
                arr.push(arr[current] + arr[next++])
                current++;
                return {
                    value: arr.length <= n ? arr : undefined,
                    done: current > n,
                };
            }
        };
    }
};