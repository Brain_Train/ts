class RestorauntCalculation {
    restorauntData;
    constructor(data: IEmployeers[]) {
        this.restorauntData = data;
    }
    
    countingSalariesEachDepartment() {
        let allSalary: Record<string, number> = {};
        
        this.restorauntData.forEach((value) => {
            allSalary[value.department] = allSalary[value.department] || 0;
            allSalary[value.department] += value.salary;
        });
        
        return allSalary;
    }

    countingAverageSalaryDepartment() {
        let avarageSalary: Record<string, number[]> = {};
        let result:  Record<string, number> = {};

        this.restorauntData.forEach((item) => {
            avarageSalary[item.department] = avarageSalary[item.department] || [];

            for (let i in avarageSalary) {
                if (i === item.department) {
                    avarageSalary[i].push(item.salary);
                }
            }
        });

        function average(name: string, items: any) {
            let sum = 0;

            for (let i = 0; i < items.length; i++) {
                sum += items[i];
            }

            result[name] = Math.round(sum / items.length);
        }

        for (let index in avarageSalary) {
            average(index, avarageSalary[index]);
        }

        return result;
    }

    menedgerFired(post: string) {
        let result: Record<string, number> = {};

        this.restorauntData.forEach((value) => {
            result[value.department] = result[value.department] || 0;

            if (value.fired === true && value.post === post) {
                ++result[value.department];
            }
        });
        
        return result;
    }

    firedAmount(isFired: boolean) {
        let result: Record<string, number> = {};

        this.restorauntData.forEach((value) => {
            result[value.department] = result[value.department] || 0;
            
            if (value.fired === !isFired) {
                ++result[value.department];
            }
        });

        return result;
    }

}

const restorauntResult = new RestorauntCalculation(employerArr);

