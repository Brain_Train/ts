class RenderBank {
    bankData: IBank[];
    modalSwitch: boolean | null;
    buttonOpenModalBank: HTMLElement;
    allCard: HTMLElement;
    modalBank: HTMLElement;
    modalCardBank: HTMLElement;
    information: HTMLElement;
    constructor(data: IBank[]) {
        this.bankData = data;
        this.modalSwitch = null;
        this.buttonOpenModalBank = document.querySelector('.apperButton') as HTMLElement;
        this.allCard = document.querySelector('.blockCardBank') as HTMLElement;
        this.modalBank = document.querySelector('.modalBank') as HTMLElement;
        this.modalCardBank = document.querySelector('.modalBank__modalCardBank') as HTMLElement;
        this.information = document.querySelector('.allInfo') as HTMLElement;
        this.renderCard();
        this.informations();
    }

    informations() {
        let allMoney = bankResult.countingMoney();
        let allBorovedMoney = bankResult.countingBorowedMoney();
        let allBorowedMonyeInaciveUsers = bankResult.countingBorowedMonyeInaciveUsers();

        const totalMoney = document.createElement('div');
        totalMoney.className = 'totalMoney';

        allMoney.then((result: string) => {
            totalMoney.innerHTML =  `
            <p class="information"> Total money: ${result} RUB</p>
            `;
        });

        const totalBorovedMoney = document.createElement('div');
        totalBorovedMoney.className = 'totalBorovedMoney';

        allBorovedMoney.then((result: string) => {
            totalBorovedMoney.innerHTML =  `
            <p class="information"> Total boroved money: ${result} RUB</p>
            `;
        });

        const totalBorovedInactiveUsers = document.createElement('div');
        totalBorovedInactiveUsers.className = 'totalBorovedInactiveUsers';

        allBorowedMonyeInaciveUsers.then((result: string) => {
            totalBorovedInactiveUsers.innerHTML =  `
            <p class="information"> Total boroved money inactive users: ${result} RUB</p>
            `;
        });


        this.information?.append(totalMoney);
        this.information?.append(totalBorovedMoney);
        this.information?.append(totalBorovedInactiveUsers);
    }

    createCards(data: IBank, index: number) {
        const card: HTMLDivElement = document.createElement('div');
        card.className = 'cardBanks';
        card.innerHTML = `
        <h3 class="allLable">${data.name} ${data.surname}</h3>
        <p class="infText">Balance: ${data.credit.ownBalance.ownBalance} ${data.credit.ownBalance.currency}</p>
        <p class="infText">CreditBalance: ${data.credit.creditBalance.creditBalance} ${data.credit.creditBalance.currency}</p>
        <p class="infText">Credit Limit: ${data.credit.creditBalance.creditLimit} ${data.credit.creditBalance.currency}</p>
        <p class="infText">Debit: ${data.debit.curentBalance} ${data.debit.currency}</p>
        <p class="infText">Activeted data: ${data.credit.ownBalance.activetedData}</p>
        <p class="infText">Expired data: ${data.credit.ownBalance.expiredData}</p>
        <p class="infText">status: ${data.activeUser ? 'active' : 'disabled'}</p>
        `;

        const buttonChange: HTMLButtonElement = document.createElement('button');
        buttonChange.className = 'cardButtonBank';
        buttonChange.innerHTML = 'Change';
        card.appendChild(buttonChange);
        buttonChange.addEventListener('click', this.openChangingModal.bind(this, data, index));

        this.buttonOpenModalBank?.addEventListener('click', this.openCreatingModal.bind(this, data));

        const buttonDelete: HTMLButtonElement = document.createElement('button');
        buttonDelete.className = 'deleteUserButton';
        buttonDelete.innerHTML = 'Delete';
        card.appendChild(buttonDelete);
        buttonDelete.addEventListener('click', this.deleteUsers.bind(this, index));

        return card;
    }

    openModal(data: IBank, index: number) {
        this.modalBank?.classList.add('active');
        this.modalCardBank!.innerHTML = `
        <div class="changes">
            <p> Name </p>
            <input class="inputChanges" type="text" placeholder="Enter name..." name="name"> 
        </div>
        <div class="changes">
            <p> Surname </p>
            <input class="inputChanges" type="text" placeholder="Enter surname..." name="surname"> 
        </div>
        <div class="changes">
            <p> Balance </p>
            <input class="inputChanges" type="text" placeholder="Enter Balance..." name="balance"> 
        </div>
        <div class="changes">
            <p> Currency </p>
            <input class="inputChanges" type="text" placeholder="Enter Currency..." name="currency"> 
        </div>
        <div class="changes">
            <p> Сredit Balance </p>
            <input class="inputChanges" type="text" placeholder="Enter Сredit Balance..." name="creditBalance"> 
        </div>
        <div class="changes">
            <p> Сredit Limit </p>
            <input class="inputChanges" type="text" placeholder="Enter Сredit Limit..." name="creditLimit"> 
        </div>
        <div class="changes">
            <p> Debit </p>
            <input class="inputChanges" type="text" placeholder="Enter Debit..." name="debit"> 
        </div>
        <div class="changes">
            <p> Activeted Data </p>
            <input class="inputChanges" type="text" placeholder="Enter Activeted Data..." name="activetedData"> 
        </div>
        <div class="changes">
            <p> Expired Data </p>
            <input class="inputChanges" type="text" placeholder="Enter Expired Data..." name="expiredData"> 
        </div>
        <div class="changes">
            <p> Status (active/disabled) </p>
            <input class="check" type="checkbox" placeholder="Enter Status..." name="activeUser"> 
        </div>
        `;

        const divForButton = document.createElement('div');
        divForButton.className = 'forButton';
        this.modalCardBank?.append(divForButton);

        const button = document.createElement('button');
        button.className = 'editeEmploeerButton';
        button.innerText = 'Apply changes';
        divForButton.appendChild(button);

        divForButton.addEventListener('click', this.editeEmployeer.bind(this, index));

        this.modalCardBank?.addEventListener('click', this.stopPropagation.bind(this));
        this.modalBank?.addEventListener('click', this.closeModal.bind(this));
    }
    
    createUser(event: any) {
        event.preventDefault();
        
        let newEmpoloyeer = new FormData(event.target.closest('form'));  

        this.bankData.unshift({
            name: String(newEmpoloyeer.get('name')) || 'Name',
            surname: String(newEmpoloyeer.get('surname')) || 'Surname',
            activeUser: Boolean(newEmpoloyeer.get('activeUser')) || false,
            credit: {
                ownBalance: {
                    ownBalance: Number(newEmpoloyeer.get('balance')) || 0,
                    activetedData: String(newEmpoloyeer.get('activetedData')) || 'Activeted Data',
                    expiredData: String(newEmpoloyeer.get('expiredData')) || 'Expired Data',
                    currency: String(newEmpoloyeer.get('currency')) || 'RUB',
                    active: Boolean(newEmpoloyeer.get('activeUser')) || false,
                },
                creditBalance: {
                    creditBalance: Number(newEmpoloyeer.get('creditBalance')) || 0,
                    creditLimit: Number(newEmpoloyeer.get('creditLimit')) || 0,
                    active: Boolean(newEmpoloyeer.get('activeUser')) || false,
                    activetedData: String(newEmpoloyeer.get('activetedData')) || 'Activeted Data',
                    expiredData: String(newEmpoloyeer.get('expiredData')) || 'Expired Data', 
                    currency: String(newEmpoloyeer.get('currency')) || 'RUB',
                },
            },
            debit: {
                curentBalance: Number(newEmpoloyeer.get('debit')) || 0,  
                active: Boolean(newEmpoloyeer.get('activeUser')) || false,
                activetedData: String(newEmpoloyeer.get('activetedData')) || 'Activeted Data',
                expiredData: String(newEmpoloyeer.get('expiredData')) || 'Expired Data',
                currency: String(newEmpoloyeer.get('currency')) || 'RUB',
            }
        });
        this.renderCard();
    
    }

    changeUsers(event: any, i: number) {
        event.preventDefault();

        let changeEmployeer = new FormData(event.target.closest('form'));
        
        this.bankData[i].name = String(changeEmployeer.get('name')) || this.bankData[i].name;
        this.bankData[i].surname = String(changeEmployeer.get('surname')) || this.bankData[i].surname;
        this.bankData[i].credit.ownBalance.ownBalance = Number(changeEmployeer.get('balance')) || this.bankData[i].credit.ownBalance.ownBalance;
        this.bankData[i].credit.creditBalance.creditBalance = Number(changeEmployeer.get('creditBalance')) || this.bankData[i].credit.creditBalance.creditBalance;
        this.bankData[i].credit.creditBalance.creditLimit = Number(changeEmployeer.get('creditLimit')) || this.bankData[i].credit.creditBalance.creditLimit;
        this.bankData[i].credit.ownBalance.activetedData = String(changeEmployeer.get('activetedData')) || this.bankData[i].credit.ownBalance.activetedData;
        this.bankData[i].credit.ownBalance.expiredData = String(changeEmployeer.get('expiredData')) || this.bankData[i].credit.ownBalance.expiredData;
        this.bankData[i].credit.ownBalance.active = Boolean(changeEmployeer.get('activeUser')) || this.bankData[i].credit.ownBalance.active;

        this.renderCard();
        
    }

    renderCard() {
        this.allCard!.innerHTML = ' ';
        const cards = this.bankData.map((data, index) => this.createCards(data, index));
        this.allCard!.append(...cards); 
    }

    deleteUsers(index: number) {
        this.bankData.splice(index, 1);
        this.renderCard();
    }

    closeModal() {
        this.modalBank?.classList.remove('active');
    }

    openChangingModal(data: IBank, index: number) {
        this.modalSwitch = true;
        this.openModal(data, index);
    }

    editeEmployeer(index: number, event: any) {
        this.information!.innerHTML = ' ';
        if(this.modalSwitch === true){
            this.changeUsers(event, index);
        } else if(this.modalSwitch === false) {
            this.createUser(event);
        }
        this.closeModal();
        this.informations();
    }

    openCreatingModal(data: IBank, index: any) {
        this.modalSwitch = false;
        this.openModal(data, index);
    }

    stopPropagation(element: any) {
        element.stopPropagation();
    }
}

const renderBank = new RenderBank(bankUsers);


